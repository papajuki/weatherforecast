package com.homecredit.weather.service;

import java.util.Optional;

import com.homecredit.weather.dto.OpenWeatherResponse;

public interface OpenWeatherMapService {

  Optional<OpenWeatherResponse> getWeatherDataByCityName(String name);
  
}
