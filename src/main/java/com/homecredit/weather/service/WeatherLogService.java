package com.homecredit.weather.service;

import com.homecredit.weather.bean.WeatherLog;

public interface WeatherLogService {

  WeatherLog save(WeatherLog weatherLog);
  
}
