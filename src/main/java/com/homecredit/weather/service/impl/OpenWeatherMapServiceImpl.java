package com.homecredit.weather.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.homecredit.weather.dto.OpenWeatherResponse;
import com.homecredit.weather.service.OpenWeatherMapService;

import lombok.extern.log4j.Log4j2;

@Log4j2
@Service
public class OpenWeatherMapServiceImpl implements OpenWeatherMapService {

  @Value("${openweathermap.api.data.city.name.url}")
  private String weatherDataByCityNameUrl;
  
  @Value("${openweathermap.api.access.key}")
  private String openWeatherMapApiKey;
  
  private RestTemplate restTemplate = new RestTemplate();
  
  public OpenWeatherMapServiceImpl() {
    super();
  }

  @Override
  public Optional<OpenWeatherResponse> getWeatherDataByCityName(String name) {
    
    log.debug("Using RestTemplate to get WeatherDataByCityName with name: {}", name);  
    
    UriComponentsBuilder builder = UriComponentsBuilder
        .fromUriString(weatherDataByCityNameUrl)
        .queryParam("q", name.replaceAll(" ", "+"))
        .queryParam("APPID", openWeatherMapApiKey);
    
    try {
      return Optional.of(restTemplate.getForObject(builder.toUriString(), OpenWeatherResponse.class));
    } catch(HttpClientErrorException e) {
      log.error("Failed getWeatherDataByCityName. Exception? {}", e);
      return Optional.empty();
    }
  }

}
