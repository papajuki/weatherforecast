package com.homecredit.weather.service.impl;

import org.springframework.stereotype.Service;

import com.homecredit.weather.bean.WeatherLog;
import com.homecredit.weather.repository.WeatherLogRepository;
import com.homecredit.weather.service.WeatherLogService;

@Service
public class WeatherLogServiceImpl implements WeatherLogService {

  private final WeatherLogRepository weatherLogRepository;
  
  public WeatherLogServiceImpl(WeatherLogRepository weatherLogRepository) {
    super();
    this.weatherLogRepository = weatherLogRepository;
  }

  @Override
  public WeatherLog save(WeatherLog weatherLog) {
    return weatherLogRepository.save(weatherLog);
  }

}
