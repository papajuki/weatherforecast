package com.homecredit.weather.bean;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "WeatherLog")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class WeatherLog {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id")
  private long id;
  
  @GeneratedValue(generator = "uuid2")
  @Column(name = "responseId", unique=true)
  private String responseId;
  
  @Column(name = "location")
  private String location;
  
  @Column(name = "actualWeather")
  private String actualWeather;
  
  @Column(name = "temperature")
  private String temperature;
  
  @Column(name = "dtimeInserted")
  private LocalDateTime dtimeInserted;
  
}
