package com.homecredit.weather.controller;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.homecredit.weather.bean.WeatherLog;
import com.homecredit.weather.dto.ApiWeatherResponse;
import com.homecredit.weather.dto.ApiWeatherResponseList;
import com.homecredit.weather.dto.WeatherResponse;
import com.homecredit.weather.service.OpenWeatherMapService;
import com.homecredit.weather.service.WeatherLogService;
import com.homecredit.weather.util.CrudUtil;

import lombok.extern.log4j.Log4j2;

@Log4j2
@RestController
@RequestMapping("/api/v1/weather")
public class WeatherController {

  @Value("${openweathermap.api.cities}")
  private String propertyCities;
  
  @Value("${openweathermap.api.count.unique.last}")
  private int lastUniqueResponseCounts;
  
  private final OpenWeatherMapService openWeatherMapService;
  private final WeatherLogService weatherLogService;
  
  private List<ApiWeatherResponseList> bankOfWeatherResponses = new ArrayList<>();
  
  public WeatherController(OpenWeatherMapService openWeatherMapService, WeatherLogService weatherLogService) {
    super();
    this.openWeatherMapService = openWeatherMapService;
    this.weatherLogService = weatherLogService;
  }

  /*
   * End-point checker to see if per CityName is working
   */
  @GetMapping("/data/{cityName}")
  public ResponseEntity<ApiWeatherResponse> getWeatherDataByCityName(@PathVariable String cityName) {
    
    log.debug("Retrieving Weather Data by cityName: {}", cityName);

    ApiWeatherResponse apiWeatherResponse = ApiWeatherResponse.builder().build();
    
    return openWeatherMapService.getWeatherDataByCityName(cityName)
        .map(
              openWeatherResponse -> {
                apiWeatherResponse.setLocation(openWeatherResponse.getName());
                apiWeatherResponse.setActualWeather(getActualWeatherCombined(openWeatherResponse.getWeather()));
                apiWeatherResponse.setTemperature(openWeatherResponse.getMain().getTemp());
                return new ResponseEntity<>(apiWeatherResponse, HttpStatus.OK);
              }
        )
        .orElse(ResponseEntity.notFound().build());
    
  }
  
  /*
   * API#1 - Displays list of weather information from London, Prague and San Francisco.
   * 
   * For dynamic solution - cities are defined in `application.yml` file
   * see -> openweathermap.api.cities
   * 
   * @author JunKingMinon
   */
  @GetMapping("/data/property/cities")
  public ResponseEntity<List<ApiWeatherResponse>> getWeatherDataByPropertyCities() {
    
    log.debug("Retrieving Weather Data by cities under application.yml file: {}", propertyCities);
    
    if(!StringUtils.isBlank(propertyCities)) {
      
      // convert to list the cities from the comma-delimited application.yml
      List<String> cities = Arrays.asList(propertyCities.split(","));
      List<ApiWeatherResponse> apiWeatherResponses = new ArrayList<>();
      
      apiWeatherResponses.addAll(
          // stream the List<String> to List<Optional<ApiWeatherResponse>> to List<ApiWeatherResponse>  
          cities.stream().map(city -> 
            openWeatherMapService.getWeatherDataByCityName(city).map(
              openWeatherResponse -> {
                  ApiWeatherResponse apiWeatherResponse = ApiWeatherResponse.builder().build();
                  apiWeatherResponse.setLocation(openWeatherResponse.getName());
                  apiWeatherResponse.setActualWeather(getActualWeatherCombined(openWeatherResponse.getWeather()));
                  apiWeatherResponse.setTemperature(openWeatherResponse.getMain().getTemp());
                  apiWeatherResponse.setDt(openWeatherResponse.getDt());
                  return apiWeatherResponse;
              }
            )
          ).filter(Optional::isPresent).map(Optional::get).collect(Collectors.toList()));
      
      // update the bank of weatherResponses for API#2
      if(validUniqueResponses(apiWeatherResponses)) {
        bankOfWeatherResponses.add(
            ApiWeatherResponseList.builder().apiWeatherResponses(apiWeatherResponses).build());
        log.debug("Valid unique. Added to bank of weather responses. Size: {} Bank:{}", 
            bankOfWeatherResponses.size(), bankOfWeatherResponses);
      }

      return new ResponseEntity<>(apiWeatherResponses, HttpStatus.OK);
    }      
    else {
      log.error("Found no configured cities under application.yml file.");
      return ResponseEntity.notFound().build();    
    }   
  }

  /*
   * API#2 - Store last five unique responses of API #1. Information should be saved in DB Table
   * 
   * @author JunKingMinon
   */
  @PostMapping("/data/store/last-five-unique")
  public ResponseEntity<List<ApiWeatherResponseList>> storeLastNUniqueApiOneResponses() {
    
    log.debug("Current bank of WeatherRespones has a size of {}", bankOfWeatherResponses.size());
    
    if(!bankOfWeatherResponses.isEmpty()) {
      
      // get the last N elements of bankOfWeatherResponses -> defined in property file
      bankOfWeatherResponses = bankOfWeatherResponses.subList(
                                        Math.max(bankOfWeatherResponses.size() - lastUniqueResponseCounts, 0), 
                                            bankOfWeatherResponses.size());
      
      saveBankOfWeatherResponses(bankOfWeatherResponses);
      
      return new ResponseEntity<>(bankOfWeatherResponses, HttpStatus.OK);      
    }      
    else {
      log.debug("Found no unique responses. No calls yet under API#1");
      return ResponseEntity.notFound().build();    
    }   
  }

  /*
   * Helper class to combine multiple weather conditions for a given city
   */
  private String getActualWeatherCombined(List<WeatherResponse> weatherList) {
    return weatherList.stream()
                      .map(WeatherResponse::getDescription)
                      .collect(Collectors.joining(" with "));
  }
  
  /*
   *  Helper class to identify validity of uniqueness
   *  since we're basing `uniqueness` of a response for each location on DT(Time of data calculation)
   *  we just have to check if the last inserted element to the bank matches the new response from API#1
   *  
   */
  private boolean validUniqueResponses(List<ApiWeatherResponse> apiWeatherResponses) {
 
    if(!bankOfWeatherResponses.isEmpty()) {
      
      // map the List<ApiWeatherResponse> to Map<String, String>
      Map<String, String> dtPerLocationMap = apiWeatherResponses.stream().collect(
              Collectors.toMap(ApiWeatherResponse::getLocation, ApiWeatherResponse::getDt));
      
      // better time complexity using Maps
      return bankOfWeatherResponses
              .get(bankOfWeatherResponses.size() - 1)
              .getApiWeatherResponses()
              .stream()
              .noneMatch(apiWeatherResponse -> 
                 dtPerLocationMap.containsKey(apiWeatherResponse.getLocation()) &&
                   dtPerLocationMap.get(apiWeatherResponse.getLocation()).equals(apiWeatherResponse.getDt()));
    }      
    else {
      // bank is still empty thus new response will always be unique
      return true;     
    }

  }
  
  /*
   * Helper class to save the Weather Data for each country
   * flattened first the List<ApiWeatherResponseList> or List<List<ApiWeatherResponse>>
   *           INTO List<ApiWeatherResponse> before iteration to save 
   * 
   */
  private void saveBankOfWeatherResponses(List<ApiWeatherResponseList> weatherResponseBanks) {

    weatherResponseBanks.stream()
                        .flatMap(weatherResponseBank -> weatherResponseBank.getApiWeatherResponses().stream())
                        .collect(Collectors.toList())
                        .forEach(
                            apiWeatherResponse -> {
                              WeatherLog weatherLog = WeatherLog.builder()
                                                                .dtimeInserted(LocalDateTime.now())
                                                                .responseId(UUID.randomUUID().toString())
                                                                .build();
                              CrudUtil.cloneSkipNull(apiWeatherResponse, weatherLog);
                              log.debug("Saving new WeatherLog: {}", weatherLog);
                              weatherLogService.save(weatherLog);
                            }
                        );
  }

}
