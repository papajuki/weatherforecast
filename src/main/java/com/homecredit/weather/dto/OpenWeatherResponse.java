package com.homecredit.weather.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class OpenWeatherResponse {

  private List<WeatherResponse> weather;
  private MainResponse main;
  private String name;
  private String dt;                        // Time of data calculation - identifies uniqueness
  
}
