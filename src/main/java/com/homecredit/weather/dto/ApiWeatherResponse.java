package com.homecredit.weather.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@JsonPropertyOrder({ "location", "actualWeather", "temperature" })
public class ApiWeatherResponse {

  private String location;
  
  @JsonProperty("actual_weather")
  private String actualWeather;
  
  private String temperature;
  
  @JsonIgnore
  private String dt;              // Time of data calculation - identifies uniqueness
  
}
