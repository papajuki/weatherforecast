package com.homecredit.weather.util;

import static org.springframework.beans.PropertyAccessorFactory.forBeanPropertyAccess;

import java.beans.PropertyDescriptor;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang3.ArrayUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeanWrapper;

/*
 * Static helper methods to perform CRUD operations
 * as of now just cloning :)
 * 
 * @author JunKingMinon
 *
 */
public class CrudUtil {
  
  private CrudUtil() {
    throw new IllegalStateException("Crud Utility class only!");
  }
  
  /*
   * clone object SRC props to DEST props 
   * without custom skipFields but ignores NULL props
   */
  public static Object cloneSkipNull(Object src, Object dest) {
  
    return cloneSkipNull(src, dest, new String[] { });
  }
  
  /*
   * clone object SRC props to DEST props 
   * WITH skipFields and ignore NULL props
   */
  public static Object cloneSkipNull(Object src, Object dest, String[] skipFields) {
    
    String[] ignoredProperties = ArrayUtils.addAll( skipFields, getNullPropertyNames( src ) );     
    BeanUtils.copyProperties(src, dest, ignoredProperties );
    return dest;
    
  }
  
  /*
   * helper method to get object props with null values
   */
  public static String[] getNullPropertyNames (Object source) {
    
      BeanWrapper srcWrapper = forBeanPropertyAccess(source);
      PropertyDescriptor[] props = srcWrapper.getPropertyDescriptors();

      Set<String> nullProperties = new HashSet<>();
      
      for(PropertyDescriptor prop : props) {
        if( !srcWrapper.isReadableProperty( prop.getName() ) )
                continue;
          Object srcValue = srcWrapper.getPropertyValue(prop.getName());
          if (srcValue == null) 
            nullProperties.add(prop.getName());
      }
      
      String[] result = new String[nullProperties.size()];
      return nullProperties.toArray(result);
  }
}
