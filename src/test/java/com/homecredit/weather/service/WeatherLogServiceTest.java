package com.homecredit.weather.service;

import static org.mockito.Mockito.doReturn;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.homecredit.weather.bean.WeatherLog;
import com.homecredit.weather.repository.WeatherLogRepository;

import lombok.extern.log4j.Log4j2;

/*
 * TDD Sample for WeatherLogService
 * 
 * @author JunKingMinon
 *
 */
@Log4j2
@ExtendWith(SpringExtension.class)
@SpringBootTest
@ActiveProfiles("test")
public class WeatherLogServiceTest {

  @MockBean
  private WeatherLogRepository weatherLogRepository;
  
  @Autowired
  private WeatherLogService weatherLogService;
  
  @Test
  @DisplayName("Save: Test Success")
  void testSaveSuccess() {
    
    log.debug("Save: Test Success");
    
    // mocked the bean and repository
    WeatherLog mockWeatherLog = WeatherLog.builder()
                                           .id(1)
                                           .location("London")
                                           .build();
    
    doReturn(mockWeatherLog).when(weatherLogRepository).save(mockWeatherLog);

    // execute the service call
    WeatherLog returnedProd = weatherLogService.save(mockWeatherLog);
    
    // assert the returned object
    Assertions.assertNotNull(returnedProd, "WeatherLog save should not be null");
    
    log.debug("============================================================");
  }
  
}
