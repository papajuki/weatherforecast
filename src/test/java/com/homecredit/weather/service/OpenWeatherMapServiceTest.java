package com.homecredit.weather.service;

import java.util.Optional;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.homecredit.weather.dto.OpenWeatherResponse;

import lombok.extern.log4j.Log4j2;

/*
 * TDD Sample for OpenWeatherMapService using WiremockMappings
 * 
 * @author JunKingMinon
 *
 */
@Log4j2
@ExtendWith(SpringExtension.class)
@SpringBootTest
@ActiveProfiles("test")
public class OpenWeatherMapServiceTest {

  @Autowired
  private OpenWeatherMapService openWeatherMapService;

  private WireMockServer wireMockServer;
  
  @BeforeEach
  void beforeEach() {
    
    log.debug("Starting WireMock server...");
    
    // start WireMock Server
    wireMockServer = new WireMockServer(9999);
    wireMockServer.start();
    
  }
  
  @AfterEach
  void afterEach() {
    wireMockServer.stop();
  }
  
  @Test
  @DisplayName("Get Weather Forecast By City: Test Success")
  void testGetWeatherDataByCityNameSuccess() {
    
    log.debug("Get Weather Forecast By City: Test Success");
    
    // execute the service call
    Optional<OpenWeatherResponse> response = openWeatherMapService.getWeatherDataByCityName("London");
    
    // validate the response
    Assertions.assertTrue(response.isPresent(), "Weather Data Response should be present!");
    Assertions.assertEquals("London", response.get().getName(), "City Name should be equal!");
    Assertions.assertEquals("274.34", response.get().getMain().getTemp(), "Temperature should be equal!");
    Assertions.assertEquals(3, response.get().getWeather().size(), "Weather conditions should match the number!");
    
    log.debug("============================================================");
  }
  
  @Test
  @DisplayName("Get Weather Forecast By City: Test Not Found")
  void testGetWeatherDataByCityNameNotFound() {
    
    log.debug("Get Weather Forecast By City: Test Not Found");
    
    // execute the service call
    Optional<OpenWeatherResponse> response = openWeatherMapService.getWeatherDataByCityName("Manila");
    
    // validate the response
    Assertions.assertFalse(response.isPresent(), "Weather Data Response should NOT be present!");
    
    log.debug("============================================================");
  }
  
}
