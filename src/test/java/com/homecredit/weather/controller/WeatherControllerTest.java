package com.homecredit.weather.controller;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Mockito.doReturn;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import com.homecredit.weather.dto.ApiWeatherResponse;
import com.homecredit.weather.dto.ApiWeatherResponseList;
import com.homecredit.weather.dto.MainResponse;
import com.homecredit.weather.dto.OpenWeatherResponse;
import com.homecredit.weather.dto.WeatherResponse;
import com.homecredit.weather.service.OpenWeatherMapService;

import lombok.extern.log4j.Log4j2;

/*
 * TDD for WeatherController
 * 
 * @author JunKingMinon
 *
 */
@Log4j2
@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class WeatherControllerTest {
  
  @Value("${openweathermap.api.cities}")
  private String propertyCities;
  
  @Value("${openweathermap.api.count.unique.last}")
  private int lastUniqueResponseCounts;
  
  @MockBean
  private OpenWeatherMapService openWeatherMapService;
  
  @Autowired
  private MockMvc mockMvc;
  
  @Test
  @DisplayName("Get Weather Forecast By City: Test Success")
  void testGetWeatherByCity() throws Exception {
  
    log.debug("Get Weather Forecast By City: Test Success");
    
    // mock the response from OpenWeatherMap
    MainResponse mainResponse = MainResponse.builder().temp("283.32").build();
    WeatherResponse weatherResponse1 = WeatherResponse.builder().description("fog").build();
    WeatherResponse weatherResponse2 = WeatherResponse.builder().description("mist").build();
    OpenWeatherResponse response = OpenWeatherResponse.builder()
                                                      .name("London")
                                                      .weather(Arrays.asList(weatherResponse1, weatherResponse2))
                                                      .main(mainResponse)
                                                      .build();
    // mock the service 
    doReturn(Optional.of(response)).when(openWeatherMapService).getWeatherDataByCityName("London");
    
    // mock the MVC
    mockMvc.perform(get("/api/v1/weather/data/{cityName}", "London"))
    
      // validates response code && content type
      .andExpect(status().isOk())
      .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))

      // validates body fields
      .andExpect(jsonPath("$.location", is("London")))
      .andExpect(jsonPath("$.actual_weather", is("fog with mist")))
      .andExpect(jsonPath("$.temperature", is("283.32")));
    
    log.debug("============================================================");
  }
  
  @Test
  @DisplayName("Get Weather Forecast By Cities using Properties: Test Success")
  void testGetWeatherByCitiesUnderProperties() throws Exception {
  
    log.debug("Get Weather Forecast By Cities using Properties: Test Success");
    
    // mock the responses from OpenWeatherMap
    MainResponse mainResponse1 = MainResponse.builder().temp("283.32").build();
    MainResponse mainResponse2 = MainResponse.builder().temp("222").build();
    MainResponse mainResponse3 = MainResponse.builder().temp("212").build();
    WeatherResponse weatherResponse1 = WeatherResponse.builder().description("fog").build();
    WeatherResponse weatherResponse2 = WeatherResponse.builder().description("mist").build();
    WeatherResponse weatherResponse3 = WeatherResponse.builder().description("clear sky").build();
    OpenWeatherResponse response1 = OpenWeatherResponse.builder()
                                                      .name("London")
                                                      .weather(Arrays.asList(weatherResponse1))
                                                      .main(mainResponse1)
                                                      .build();
    OpenWeatherResponse response2 = OpenWeatherResponse.builder()
                                                      .name("Prague")
                                                      .weather(Arrays.asList(weatherResponse2))
                                                      .main(mainResponse2)
                                                      .build();
    OpenWeatherResponse response3 = OpenWeatherResponse.builder()
                                                      .name("San Francisco")
                                                      .weather(Arrays.asList(weatherResponse3))
                                                      .main(mainResponse3)
                                                      .build();
    // mock the services 
    doReturn(Optional.of(response1)).when(openWeatherMapService).getWeatherDataByCityName("London");
    doReturn(Optional.of(response2)).when(openWeatherMapService).getWeatherDataByCityName("Prague");
    doReturn(Optional.of(response3)).when(openWeatherMapService).getWeatherDataByCityName("San Francisco");
    
    // mock the MVC
    mockMvc.perform(get("/api/v1/weather/data/property/cities"))
    
      // validates response code && content type
      .andExpect(status().isOk())
      .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))

      // validates body fields
      .andExpect(jsonPath("$[0].location", is("London")))
      .andExpect(jsonPath("$[1].location", is("Prague")))
      .andExpect(jsonPath("$[2].location", is("San Francisco")))
      .andExpect(jsonPath("$[0].actual_weather", is("fog")))
      .andExpect(jsonPath("$[1].actual_weather", is("mist")))
      .andExpect(jsonPath("$[2].actual_weather", is("clear sky")))
      .andExpect(jsonPath("$[0].temperature", is("283.32")))
      .andExpect(jsonPath("$[1].temperature", is("222")))
      .andExpect(jsonPath("$[2].temperature", is("212")));
    
    log.debug("============================================================");
  }
  
  @Test
  @DisplayName("Store latest N unique API#1 responses: Test Success")
  void testApiNumber2() throws Exception {
  
    log.debug("Store latest N unique API#1 responses: Test Success");
    
    // mock the responses from OpenWeatherMap
    MainResponse mainResponse1 = MainResponse.builder().temp("283.32").build();
    MainResponse mainResponse2 = MainResponse.builder().temp("222").build();
    MainResponse mainResponse3 = MainResponse.builder().temp("212").build();
    WeatherResponse weatherResponse1 = WeatherResponse.builder().description("fog").build();
    WeatherResponse weatherResponse2 = WeatherResponse.builder().description("mist").build();
    WeatherResponse weatherResponse3 = WeatherResponse.builder().description("clear sky").build();
    OpenWeatherResponse response1 = OpenWeatherResponse.builder()
                                                      .name("London")
                                                      .weather(Arrays.asList(weatherResponse1))
                                                      .main(mainResponse1)
                                                      .build();
    OpenWeatherResponse response2 = OpenWeatherResponse.builder()
                                                      .name("Prague")
                                                      .weather(Arrays.asList(weatherResponse2))
                                                      .main(mainResponse2)
                                                      .build();
    OpenWeatherResponse response3 = OpenWeatherResponse.builder()
                                                      .name("San Francisco")
                                                      .weather(Arrays.asList(weatherResponse3))
                                                      .main(mainResponse3)
                                                      .build();
    // mock the services 
    doReturn(Optional.of(response1)).when(openWeatherMapService).getWeatherDataByCityName("London");
    doReturn(Optional.of(response2)).when(openWeatherMapService).getWeatherDataByCityName("Prague");
    doReturn(Optional.of(response3)).when(openWeatherMapService).getWeatherDataByCityName("San Francisco");

    List<ApiWeatherResponse> apiWeatherResponses = new ArrayList<>();
    List<String> cities = Arrays.asList(propertyCities.split(","));
    
    apiWeatherResponses.addAll(
        // stream the List<String> to List<Optional<ApiWeatherResponse>> to List<ApiWeatherResponse>  
        cities.stream().map(city -> 
          openWeatherMapService.getWeatherDataByCityName(city).map(
            openWeatherResponse -> {
                ApiWeatherResponse apiWeatherResponse = ApiWeatherResponse.builder().build();
                apiWeatherResponse.setLocation(openWeatherResponse.getName());
                apiWeatherResponse.setActualWeather(getActualWeatherCombined(openWeatherResponse.getWeather()));
                apiWeatherResponse.setTemperature(openWeatherResponse.getMain().getTemp());
                apiWeatherResponse.setDt(openWeatherResponse.getDt());
                return apiWeatherResponse;
            }
          )
        ).filter(Optional::isPresent).map(Optional::get).collect(Collectors.toList()));
    
    List<ApiWeatherResponseList> bankOfWeatherResponses = new ArrayList<>();
    
    bankOfWeatherResponses.addAll(Arrays.asList(
        ApiWeatherResponseList.builder().apiWeatherResponses(apiWeatherResponses).build(),
        ApiWeatherResponseList.builder().apiWeatherResponses(apiWeatherResponses).build(),
        ApiWeatherResponseList.builder().apiWeatherResponses(apiWeatherResponses).build(),
        ApiWeatherResponseList.builder().apiWeatherResponses(apiWeatherResponses).build(),
        ApiWeatherResponseList.builder().apiWeatherResponses(apiWeatherResponses).build(),
        ApiWeatherResponseList.builder().apiWeatherResponses(apiWeatherResponses).build(),
        ApiWeatherResponseList.builder().apiWeatherResponses(apiWeatherResponses).build(),
        ApiWeatherResponseList.builder().apiWeatherResponses(apiWeatherResponses).build()));
    
    // test to get the last N elements
    bankOfWeatherResponses = bankOfWeatherResponses.subList(
                                Math.max(bankOfWeatherResponses.size() - lastUniqueResponseCounts, 0), 
                                    bankOfWeatherResponses.size());
    
    Assertions.assertEquals(lastUniqueResponseCounts, bankOfWeatherResponses.size(), 
        "Size should be N after sublist");
    
    // flattened the List<ApiWeatherResponseList> <=> List<List<ApiWeatherResponse>>
    // into List<ApiWeatherResponse>
    List<ApiWeatherResponse> flatMap = bankOfWeatherResponses.stream()
                          .flatMap(weatherResponseBank -> weatherResponseBank.getApiWeatherResponses().stream())
                          .collect(Collectors.toList());
    
    Assertions.assertEquals(cities.size()*lastUniqueResponseCounts, flatMap.size(), 
        "Size should be cities.size*lastUniqueResponseCounts after flattening!");
   
    log.debug("============================================================");
  }
  
  @Test
  @DisplayName("Get Weather Forecast By City: Test Not Found")
  void testGetWeatherByCityNotFound() throws Exception {
  
    log.debug("Get Weather Forecast By City: Test Not Found");

    // mock the service 
    doReturn(Optional.empty()).when(openWeatherMapService).getWeatherDataByCityName("London");
    
    // mock the MVC
    mockMvc.perform(get("/api/v1/weather/data/{cityName}", "London"))
    
      // validates response code && content type
      .andExpect(status().isNotFound());
    
    log.debug("============================================================");
  }
  
  /*
   * Helper class to combine multiple weather conditions for a given city
   */
  private String getActualWeatherCombined(List<WeatherResponse> weatherList) {
    return weatherList.stream()
                      .map(WeatherResponse::getDescription)
                      .collect(Collectors.joining(" with "));
  }
}
