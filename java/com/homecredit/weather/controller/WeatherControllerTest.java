package com.homecredit.weather.controller;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Mockito.doReturn;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.Optional;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import com.homecredit.weather.dto.MainResponse;
import com.homecredit.weather.dto.OpenWeatherResponse;
import com.homecredit.weather.dto.WeatherResponse;
import com.homecredit.weather.service.OpenWeatherMapService;

import lombok.extern.log4j.Log4j2;

/*
 * TDD for WeatherController
 * 
 * @author JunKingMinon
 *
 */
@Log4j2
@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class WeatherControllerTest {
  
  @MockBean
  private OpenWeatherMapService openWeatherMapService;
  
  @Autowired
  private MockMvc mockMvc;
  
  @Test
  @DisplayName("Get Weather Forecast By City: Test Success")
  void testGetWeatherByCity() throws Exception {
  
    log.debug("Get Weather Forecast By City: Test Success");
    
    // mock the response from OpenWeatherMap
    MainResponse mainResponse = MainResponse.builder().temp("283.32").build();
    WeatherResponse weatherResponse1 = WeatherResponse.builder().description("fog").build();
    WeatherResponse weatherResponse2 = WeatherResponse.builder().description("mist").build();
    OpenWeatherResponse response = OpenWeatherResponse.builder()
                                                      .name("London")
                                                      .weather(Arrays.asList(weatherResponse1, weatherResponse2))
                                                      .main(mainResponse)
                                                      .build();
    // mock the service 
    doReturn(Optional.of(response)).when(openWeatherMapService).getWeatherDataByCityName("London");
    
    // mock the MVC
    mockMvc.perform(get("/api/v1/weather/data/{cityName}", "London"))
    
      // validates response code && content type
      .andExpect(status().isOk())
      .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))

      // validates body fields
      .andExpect(jsonPath("$.location", is("London")))
      .andExpect(jsonPath("$.actual_weather", is("fog with mist")))
      .andExpect(jsonPath("$.temperature", is("283.32")));
  }
  
  @Test
  @DisplayName("Get Weather Forecast By City: Test Not Found")
  void testGetWeatherByCityNotFound() throws Exception {
  
    log.debug("Get Weather Forecast By City: Test Not Found");

    // mock the service 
    doReturn(Optional.empty()).when(openWeatherMapService).getWeatherDataByCityName("London");
    
    // mock the MVC
    mockMvc.perform(get("/api/v1/weather/data/{cityName}", "London"))
    
      // validates response code && content type
      .andExpect(status().isNotFound());
  }
}
