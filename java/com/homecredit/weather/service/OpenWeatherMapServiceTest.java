package com.homecredit.weather.service;

import java.util.Optional;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;

import com.github.tomakehurst.wiremock.WireMockServer;

import lombok.extern.log4j.Log4j2;

/*
 * TDD Sample for OpenWeatherMapService using Mappings
 * 
 * @author JunKingMinon
 *
 */
@Log4j2
@SpringBootTest
@TestPropertySource(locations = "classpath:test.yml")
public class OpenWeatherMapServiceTest {

  
  @Autowired
  private OpenWeatherMapService openWeatherMapService;

  private WireMockServer wireMockServer;
  
  @BeforeEach
  void beforeEach() {
    
    // start WireMock Server
    wireMockServer = new WireMockServer(9999);
    wireMockServer.start();
    
  }
  
  @AfterEach
  void afterEach() {
    wireMockServer.stop();
  }
  
  @Test
  void testGetInventorySuccess() {
    
    // execute the service call
    Optional<InventoryRecordDTO> record = service.getInventoryRecord(1);
    
    // validate the response
    Assertions.assertTrue(record.isPresent(), "Record should be present!");
    Assertions.assertEquals(500, record.get().getQuantity().intValue(), "Record quantity does NOT match!");
  }
  
}
